import 'package:flutter/material.dart';

class ServiciosCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5),
      margin: EdgeInsets.only(top: 5, bottom: 5),
      width: double.infinity,
      height: 40,
      decoration: BoxDecoration(
          border: Border(
        bottom: BorderSide(width: 1.0, color: Colors.black12),
      )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'BIOMETRIA HEMATICA',
            style: TextStyle(color: Color.fromRGBO(39, 60, 116, 1)),
          ),
          Container(
            child: Icon(
              Icons.arrow_forward_ios,
              color: Color.fromRGBO(39, 60, 116, 1),
            ),
          )
        ],
      ),
    );
  }
}
