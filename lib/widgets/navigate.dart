import 'package:flutter/material.dart';
import 'package:horizonte_en_casa/screens/screens.dart';

class Navigate extends StatefulWidget {
  const Navigate({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return _Navigate();
  }
}

class _Navigate extends State<Navigate> {
  int indexTap = 2;
  final List<Widget> widgetsChildren = [
    ExpedienteScreen(),
    ServicesScreen(),
    ExpedienteScreen(),
    PerfilScreen(),
  ];

  void onTapTapped(int index) {
    setState(() {
      indexTap = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widgetsChildren[indexTap],
      bottomNavigationBar: Theme(
        data: Theme.of(context)
            .copyWith(canvasColor: Colors.white, primaryColor: Colors.purple),
        child: BottomNavigationBar(
          onTap: onTapTapped,
          currentIndex: indexTap,
          elevation: 0.0,
          type: BottomNavigationBarType.shifting,
          unselectedIconTheme: IconThemeData(
            color: Color.fromRGBO(39, 60, 116, 1),
          ),
          selectedItemColor: Color.fromRGBO(39, 60, 116, 1),
          items: [
            BottomNavigationBarItem(
                icon: indexTap == 0
                    ? Icon(Icons.camera_sharp)
                    : Icon(Icons.camera_outlined),
                label: "ASISTENTE"),
            BottomNavigationBarItem(
                icon: indexTap == 1
                    ? Icon(Icons.layers)
                    : Icon(Icons.layers_outlined),
                label: "SERVICIOS"),
            BottomNavigationBarItem(
                icon: indexTap == 2
                    ? Icon(Icons.favorite)
                    : Icon(Icons.favorite_outline),
                label: "EXPEDIENTE"),
            BottomNavigationBarItem(
                icon: indexTap == 3
                    ? Icon(Icons.person)
                    : Icon(Icons.person_outline),
                label: "PERFIL"),
          ],
        ),
      ),
    );
  }
}
