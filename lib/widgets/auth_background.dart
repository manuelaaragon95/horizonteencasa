import 'package:flutter/material.dart';

class AuthBackground extends StatelessWidget {
  final Widget child;

  const AuthBackground({Key? key, required this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      /* color: Colors.red, */
      width: double.infinity,
      height: double.infinity,
      child: Stack(
        children: [
          _HorizonteBox(),
          /* _HeaderLogo(), */
          this.child
        ],
      ),
    );
  }
}

class _HeaderLogo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
      margin: EdgeInsets.only(top: 10.0, bottom: 70.0, left: 30.0, right: 30.0),
      height: 220.0,
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.contain,
              image: AssetImage('assets/logo/horizonte.png'))),
    ));
  }
}

class _HorizonteBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
        width: double.infinity,
        height: size.height * 0.4,
        decoration: _purpleBackground(),
        child: Stack(
          children: [
            Positioned(
              child: _Bubble(),
              top: 90,
              left: 30,
            ),
            Positioned(
              child: _Bubble(),
              top: -40,
              left: -30,
            ),
            Positioned(
              child: _Bubble(),
              top: -50,
              right: -20,
            ),
            Positioned(
              child: _Bubble(),
              bottom: -50,
              left: 10,
            ),
            Positioned(
              child: _Bubble(),
              bottom: 120,
              right: 20,
            )
          ],
        ));
  }

  BoxDecoration _purpleBackground() => BoxDecoration(
          gradient: LinearGradient(colors: [
        Color.fromRGBO(39, 60, 116, 1),
        Color.fromRGBO(51, 69, 117, 1)
      ]));
}

class _Bubble extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );
  }
}
