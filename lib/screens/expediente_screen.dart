import 'package:flutter/material.dart';
import 'package:horizonte_en_casa/widgets/widgets.dart';

class ExpedienteScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text(
              'LABORATORIOS',
              style: TextStyle(
                color: Color.fromRGBO(39, 60, 116, 1),
              ),
            ),
          ),
          leading: IconButton(
              onPressed: () => Navigator.of(context).pop(),
              icon: Icon(
                Icons.arrow_back_ios_new,
                size: 25,
                color: Color.fromRGBO(39, 60, 116, 1),
              )),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 30, bottom: 50),
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Card(
                elevation: 0,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                shadowColor: Colors.black12,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Container(
                      margin: EdgeInsets.only(top: 15, bottom: 30),
                      child: ListView.builder(
                          itemCount: 50,
                          itemBuilder: (BuildContext context, int index) =>
                              GestureDetector(
                                onTap: () => Navigator.pushNamed(
                                    context, 'laboratorios'),
                                child: ServiciosCard(),
                              ))),
                )),
          ),
        ));
  }
}
